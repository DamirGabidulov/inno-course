package homework01;

public class Main {

    public static void main(String[] args) {
        TV tv = new TV("Samsung");
        Channel channel = new Channel("Первый");
        Program p1 = new Program("Давай поженимся");
        Program p2 = new Program("Малахов+");
        Program p3 = new Program("Пусть говорят");

        Channel channel1 = new Channel("Второй");
        Program p4 = new Program("Новости");
        Program p5 = new Program("Спорт");
        Program p6 = new Program("В мире животных");

        //вариант с отправкой полного списка каналов в телевизор
//        Channel[] myChannels = new Channel[] {channel, channel1};
//        TV tv = new TV("Sony", myChannels);

        Controller controller = new Controller(tv);

        //добавляем первый канал с программи
        tv.addChannel(channel);
        channel.addProgram(p1);
        channel.addProgram(p2);
        channel.addProgram(p3);

        //добавляем второй канал с программи
        tv.addChannel(channel1);
        channel1.addProgram(p4);
        channel1.addProgram(p5);
        channel1.addProgram(p6);

        System.out.println("Вас приветствует " + tv.getTvName());

        controller.on(1);

    }
}
