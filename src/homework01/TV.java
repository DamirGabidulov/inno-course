package homework01;

public class TV {
    private String tvName;
    private Channel[] channels;
    private int channelsCount;

    public TV(String tvName) {
        this.tvName = tvName;
        this.channels = new Channel[2];
        this.channelsCount = 0;
        // а здесь массив ограничили количеством 2 штуки внутри конструктора, поэтому начальное кол-во указали сами
    }

    public TV(String tvName, Channel[] channels) {
        this.tvName = tvName;
        this.channels = channels;
        this.channelsCount = channels.length; //так как из main пришел готовый массив, то кол-во каналов равно длине массива
    }

    public void addChannel (Channel channel){
        if (this.channelsCount < channels.length){
            this.channels[channelsCount] = channel;
            channelsCount++;
        } else {
            System.err.println("Больше каналов нет");
        }
    }

    public String getTvName() {
        return tvName;
    }

    public Channel[] getChannels() {
        return channels;
    }
}
