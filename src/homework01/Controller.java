package homework01;

public class Controller {
    private TV tv;

    public Controller(TV tv) {
        this.tv = tv;
    }

    public void on (int channelNumber){
        if (channelNumber < 1 || channelNumber > tv.getChannels().length){
            System.err.println("Такого канала не существует!");
            return;
        }
        Channel channel = tv.getChannels()[channelNumber - 1]; // "-1" потому что счет каналов на пульте начинается с первого, а в массиве с 0
        System.out.println("Вы выбрали канал - " + channel.getChannelName());
        channel.showRandomProgram();
    }
}
