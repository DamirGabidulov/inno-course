package homework01;

import java.util.Random;

public class Channel {
    private String channelName;
    private Program[] programs;
    private int programsCount;

    public Channel(String channelName) {
        this.channelName = channelName;
        this.programs = new Program[3];
        this.programsCount = 0;
    }

    public void addProgram (Program program){
        if (this.programsCount == programs.length){
            System.err.println("На сегодня программ больше нет");
            return;
        }
        programs[programsCount] = program;
        programsCount++;
    }

    public void showRandomProgram(){
        if (programsCount == 0){
            System.out.println("Канал пустой");
            return;
        }

        Random random = new Random();
        int randomIndex = random.nextInt(programsCount - 1);
        System.out.println("Идет программа - " + programs[randomIndex].getProgramName());
    }

    public String getChannelName() {
        return channelName;
    }
}
