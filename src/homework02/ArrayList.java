package homework02;

import java.lang.reflect.Array;

public class ArrayList<E> implements List<E>{
    private static final int DEFAULT_SIZE = 10;
    private E[] array;
    private int count;

    public ArrayList(Class<E> clazz) {
        this.array = (E[]) Array.newInstance(clazz, DEFAULT_SIZE);
        this.count = 0;
    }

    @Override
    public void add(E element) {
        if (count == array.length){
            resize();
        }
        array[count] = element;
        count++;
    }

    @Override
    public E get(int index) {
        if (index >= 0 && index < array.length){
            return array[index];
        } else {
            return null;
        }
    }

    private class ArrayListIterator implements Iterator<E>{

        private int currentPosition;

        @Override
        public boolean hasNext() {
            // если текущая позиция не перевалила за общее количество элементов - можно дальше
            return currentPosition < count;
        }

        @Override
        public E next() {
            // берем значение под текущей позицией итератора
            E value = array[currentPosition];
            currentPosition++;
            return value;
        }
    }

    @Override
    public Iterator<E> iterator() {
        return new ArrayListIterator();
    }

    private void resize(){
        // создаем новый массив в полтора раза больший
        E[] newArray = (E[]) Array.newInstance(Object.class, array.length + array.length/2);
        for (int i = 0; i < array.length; i++) {
            newArray[i] = array[i];
        }
        // устанавливаем ссылку на новый массив
         this.array = newArray;
    }
}
