package homework02;

public class Main {
    public static void main(String[] args) {
        List<String> stringList = new ArrayList<>(String.class);

        stringList.add("Hello");
        stringList.add("How");
        stringList.add("You");
        stringList.add("Doing?");

        Iterator<String> stringIterator = stringList.iterator();

        while (stringIterator.hasNext()){
            System.out.println(stringIterator.next());
        }
    }
}
