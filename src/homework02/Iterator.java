package homework02;

public interface Iterator<E> {
    boolean hasNext();
    E next();
}
